// Variables
var gulp = require('gulp');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');


/******** TACHES ********/

gulp.task('style', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
      .pipe(csso())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'));
});

gulp.task('html', function () {
  gulp.src('src/**/*.html')
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['style', 'html']); // a modifer